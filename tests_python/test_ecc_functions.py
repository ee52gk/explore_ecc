"""
 ▄▄▄▄▄▄▄▄  ▄      ▄  ▄▄▄▄▄▄▄▄            ▄▄▄▄▄▄▄▄  ▄      ▄
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌▌   ▐░▌▐░█▀▀▀▀▀▀            ▀▀▀██▀▀▀ ▐░▌▌   ▐░▌
▐░▌       ▐░▌▐   ▐░▌▐░▌                     ▐▌    ▐░▌▐   ▐░▌
▐░█▄▄▄▄▄▄ ▐░▌ ▌  ▐░▌▐░▌ ▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄   ▐▌    ▐░▌ ▌  ▐░▌
▐░░░░░░░░▌▐░▌ ██ ▐░▌▐░▌▐░░░░░▌▐░░░░░░░░░░▌  ▐▌    ▐░▌ ██ ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌  ▐ ▐░▌▐░▌ ▀▀▀█░▌ ▀▀▀▀▀▀▀▀▀▀   ▐▌    ▐░▌  ▐ ▐░▌
▐░▌       ▐░▌   ▌▐░▌▐░▌    ▐░▌              ▐▌    ▐░▌   ▌▐░▌
▐░█▄▄▄▄▄▄ ▐░▌   ▐▐░▌▐░█▄▄▄▄█░▌           ▄▄▄██▄▄▄ ▐░▌   ▐▐░▌
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
 ▀▀▀▀▀▀▀▀  ▀      ▀  ▀▀▀▀▀▀▀▀            ▀▀▀▀▀▀▀▀  ▀      ▀
     45       6E        67        2D        49        6E
 01000101  01101110  01100111  00101101  01001001  01101110
           ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄      ▄
          ▐░░░░░░░░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
          ▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░▌▌  ▐▐░▌
          ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌▐░░▌ ░▌
          ▐░█▄▄▄▄█░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░░░░░░░░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░█▀█░█▀▀ ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌  ▐░▌  ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌   ▐░▌ ▐░█▄▄▄▄█░▌▐░█▄▄▄▄█░▌▐░▌    ▐░▌
          ▐░▌    ▐░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
           ▀      ▀  ▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀  ▀      ▀
              52        6F        6F        6D
           01010010  01101111  01101111  01101101

Logo edited from awesomeness:
    https://patorjk.com/software/taag/#p=display&f=Electronic&t=EngIn%20Room

(C) Geoff Krechting 2021
"""

from explore_ecc_python.ecc_constants import (
    rfc5903_384_bit_Random_ECP_Group_definition,
    rfc5903_384_bit_Random_ECP_Group_binary_powers_xi,
    rfc5903_384_bit_Random_ECP_Group_binary_powers_yi,
)

from explore_ecc_python.ecc_functions import (
    euclidean_algorithm,
    modular_multiplicative_inverse,
    point_is_on_curve,
    point_double,
    point_add,
    point_multiply,
    public_key,
)


rfc5903_384_bit_Random_ECP_Group_test_vectors = {
    "i": bytes.fromhex("099F3C70 34D4A2C6 99884D73 A375A67F 7624EF7C 6B3C0F16 0647B674 14DCE655 E35B5380 41E649EE 3FAEF896 783AB194"),
    "gix": bytes.fromhex("667842D7 D180AC2C DE6F74F3 7551F557 55C7645C 20EF73E3 1634FE72 B4C55EE6 DE3AC808 ACB4BDB4 C88732AE E95F41AA"),
    "giy": bytes.fromhex("9482ED1F C0EEB9CA FC498462 5CCFC23F 65032149 E0E144AD A0241815 35A0F38E EB9FCFF3 C2C947DA E69B4C63 4573A81C"),
    "KEi": bytes.fromhex("00000068 1634FE72 FC498462 E69B4C63 00140000 667842D7 D180AC2C DE6F74F3 7551F557 55C7645C 20EF73E3 B4C55EE6 DE3AC808 ACB4BDB4 C88732AE E95F41AA 9482ED1F C0EEB9CA 5CCFC23F 65032149 E0E144AD A0241815 35A0F38E EB9FCFF3 C2C947DA 4573A81C"),
    "r": bytes.fromhex("41CB0779 B4BDB85D 47846725 FBEC3C94 30FAB46C C8DC5060 855CC9BD A0AA2942 E0308312 916B8ED2 960E4BD5 5A7448FC"),
    "grx": bytes.fromhex("E558DBEF 53EECDE3 D3FCCFC1 AEA08A89 A987475D 12FD950D 83CFA417 32BC509D 0D1AC43A 0336DEF9 6FDA41D0 774A3571"),
    "gry": bytes.fromhex("DCFBEC7A ACF31964 72169E83 8430367F 66EEBE3C 6E70C416 DD5F0C68 759DD1FF F83FA401 42209DFF 5EAAD96D B9E6386C"),
    "KEr": bytes.fromhex("00000068 83CFA417 72169E83 5EAAD96D 00140000 E558DBEF 53EECDE3 D3FCCFC1 AEA08A89 A987475D 12FD950D 32BC509D 0D1AC43A 0336DEF9 6FDA41D0 774A3571 DCFBEC7A ACF31964 8430367F 66EEBE3C 6E70C416 DD5F0C68 759DD1FF F83FA401 42209DFF B9E6386C"),
    "girx": bytes.fromhex("11187331 C279962D 93D60424 3FD592CB 9D0A926F 422E4718 7521287E 7156C5C4 D6031355 69B9E9D0 9CF5D4A2 70F59746"),
    "giry": bytes.fromhex("A2A9F38E F5CAFBE2 347CF7EC 24BDD5E6 24BC93BF A82771F4 0D1B65D0 6256A852 C983135D 4669F879 2F2C1D55 718AFBB4"),
}
rfc5903_384_bit_Random_ECP_Group_test_vectors["ii"] = int.from_bytes(
    rfc5903_384_bit_Random_ECP_Group_test_vectors['i'],
    byteorder='big', signed=False)
rfc5903_384_bit_Random_ECP_Group_test_vectors["gixi"] = int.from_bytes(
    rfc5903_384_bit_Random_ECP_Group_test_vectors['gix'],
    byteorder='big', signed=False)
rfc5903_384_bit_Random_ECP_Group_test_vectors["giyi"] = int.from_bytes(
    rfc5903_384_bit_Random_ECP_Group_test_vectors['giy'],
    byteorder='big', signed=False)
rfc5903_384_bit_Random_ECP_Group_test_vectors["ri"] = int.from_bytes(
    rfc5903_384_bit_Random_ECP_Group_test_vectors['r'],
    byteorder='big', signed=False)
rfc5903_384_bit_Random_ECP_Group_test_vectors["grxi"] = int.from_bytes(
    rfc5903_384_bit_Random_ECP_Group_test_vectors['grx'],
    byteorder='big', signed=False)
rfc5903_384_bit_Random_ECP_Group_test_vectors["gryi"] = int.from_bytes(
    rfc5903_384_bit_Random_ECP_Group_test_vectors['gry'],
    byteorder='big', signed=False)
rfc5903_384_bit_Random_ECP_Group_test_vectors["girxi"] = int.from_bytes(
    rfc5903_384_bit_Random_ECP_Group_test_vectors['girx'],
    byteorder='big', signed=False)
rfc5903_384_bit_Random_ECP_Group_test_vectors["giryi"] = int.from_bytes(
    rfc5903_384_bit_Random_ECP_Group_test_vectors['giry'],
    byteorder='big', signed=False)


class TestEccFunctions:
    def test_euclidean_algorithm(self):
        a = 42823
        b = 6409
        gcd = 17

        assert gcd, 5 == euclidean_algorithm(a, b)


    def test_modular_multiplicative_inverse(self):
        m = pi = rfc5903_384_bit_Random_ECP_Group_definition['pi']

        a = 1362138308511466522361153706999924933599454966107597910086607881313301390679204654798639248640660900363360053616481
        mmi_check = pow(a, -1, m)
        mmi_ut = modular_multiplicative_inverse(a, m)
        print()
        print(mmi_check)
        print(mmi_ut)
        assert mmi_ut == mmi_check

        a = 21933325650940841369538204578070064804451893403314136885642470114978241170633179043576249504748352841115137159204480
        mmi_check = pow(a, -1, m)
        mmi_ut = modular_multiplicative_inverse(a, m)
        print()
        print(mmi_check)
        print(mmi_ut)
        assert mmi_ut == mmi_check

        a = 3002719260124138234864644293005553694107074621611323149430212532410635475904301642043243286192603916432052661917749
        mmi_check = pow(a, -1, m)
        mmi_ut = modular_multiplicative_inverse(a, m)
        print()
        print(mmi_check)
        print(mmi_ut)
        assert mmi_ut == mmi_check

        a = 31212625517239818344942812004487160470862995565736342725949902243255222118943617085729083408956085866876436550315367
        mmi_check = pow(a, -1, m)
        mmi_ut = modular_multiplicative_inverse(a, m)
        print()
        print(mmi_check)
        print(mmi_ut)
        assert mmi_ut == mmi_check

        a = 1150902892488483458936980703033240421996917307006362560128741616924334451190275252566486993672663650518120360101937
        mmi_check = pow(a, -1, m)
        mmi_ut = modular_multiplicative_inverse(a, m)
        print()
        print(mmi_check)
        print(mmi_ut)
        assert mmi_ut == mmi_check

        a = 31026896179475916024120184231935979451435829475766013456640577102369059008378103381483562093497986423351518602141169
        mmi_check = pow(a, -1, m)
        mmi_ut = modular_multiplicative_inverse(a, m)
        print()
        print(mmi_check)
        print(mmi_ut)
        assert mmi_ut == mmi_check

        a = 18754743275811621356812366348447155036794152582973426574596694340222890660328256759016417708116066275791737346969746
        mmi_check = pow(a, -1, m)
        mmi_ut = modular_multiplicative_inverse(a, m)
        print()
        print(mmi_check)
        print(mmi_ut)
        assert mmi_ut == mmi_check

        a = 13492069884231805967150566425593616408403886574369817417579605777129408746025457363565213630285154171157504491559417
        mmi_check = pow(a, -1, m)
        mmi_ut = modular_multiplicative_inverse(a, m)
        print()
        print(mmi_check)
        print(mmi_ut)
        assert mmi_ut == mmi_check

        a = 19653438533741695225526178811955277079831496796594798312151279447697010158445447869511893059103977393986781518675387
        mmi_check = pow(a, -1, m)
        mmi_ut = modular_multiplicative_inverse(a, m)
        print()
        print(mmi_check)
        print(mmi_ut)
        assert mmi_ut == mmi_check

        try:
            a = 1
            m = 2
            mmi_ut = modular_multiplicative_inverse(a, m)
        except:
            assert False
        else:
            assert True
            assert mmi_ut == 1
            print(mmi_ut)

        try:
            a = 2
            m = 4
            mmi_ut = modular_multiplicative_inverse(a, m)
        except:
            assert False
        else:
            assert True
            assert not mmi_ut
            print(mmi_ut)


    def test_is_on_curve(self):
        assert point_is_on_curve(rfc5903_384_bit_Random_ECP_Group_definition['gxi'],
                                 rfc5903_384_bit_Random_ECP_Group_definition['gyi'],
                                 rfc5903_384_bit_Random_ECP_Group_definition)


    def test_point_double(self):
        Pxi = rfc5903_384_bit_Random_ECP_Group_definition['gxi']
        Pyi = rfc5903_384_bit_Random_ECP_Group_definition['gyi']

        pi = rfc5903_384_bit_Random_ECP_Group_definition['pi']
        ai = -3

        Rxi, Ryi = point_double(Pxi, Pyi, rfc5903_384_bit_Random_ECP_Group_definition)

        assert Rxi == 1362138308511466522361153706999924933599454966107597910086607881313301390679204654798639248640660900363360053616481
        assert Ryi == 21933325650940841369538204578070064804451893403314136885642470114978241170633179043576249504748352841115137159204480

        Pxi = Rxi
        Pyi = Ryi

        Rxi, Ryi = point_double(Pxi, Pyi, rfc5903_384_bit_Random_ECP_Group_definition)

        assert Rxi == 3002719260124138234864644293005553694107074621611323149430212532410635475904301642043243286192603916432052661917749
        assert Ryi == 31212625517239818344942812004487160470862995565736342725949902243255222118943617085729083408956085866876436550315367


    def test_point_add(self):
        Pxi = rfc5903_384_bit_Random_ECP_Group_definition['gxi']
        Pyi = rfc5903_384_bit_Random_ECP_Group_definition['gyi']

        Qxi = 1362138308511466522361153706999924933599454966107597910086607881313301390679204654798639248640660900363360053616481
        Qyi = 21933325650940841369538204578070064804451893403314136885642470114978241170633179043576249504748352841115137159204480

        pi = rfc5903_384_bit_Random_ECP_Group_definition['pi']

        Rxi, Ryi = point_add(Pxi, Pyi, Qxi, Qyi, rfc5903_384_bit_Random_ECP_Group_definition)

        assert Rxi == 1150902892488483458936980703033240421996917307006362560128741616924334451190275252566486993672663650518120360101937
        assert Ryi == 31026896179475916024120184231935979451435829475766013456640577102369059008378103381483562093497986423351518602141169

        Pxi = Rxi
        Pyi = Ryi

        Qxi = 18754743275811621356812366348447155036794152582973426574596694340222890660328256759016417708116066275791737346969746
        Qyi = 13492069884231805967150566425593616408403886574369817417579605777129408746025457363565213630285154171157504491559417

        Rxi, Ryi = point_add(Pxi, Pyi, Qxi, Qyi, rfc5903_384_bit_Random_ECP_Group_definition)

        assert Rxi == 19653438533741695225526178811955277079831496796594798312151279447697010158445447869511893059103977393986781518675387
        assert Ryi == 30520057757249756611061630760630195861591013154319688840360453776367211121110565034509569423966062967925069153533384


    def test_point_multiply(self):
        Pxi = rfc5903_384_bit_Random_ECP_Group_definition['gxi']
        Pyi = rfc5903_384_bit_Random_ECP_Group_definition['gyi']

        di = rfc5903_384_bit_Random_ECP_Group_test_vectors['ii']

        Rxi, Ryi = point_multiply(Pxi, Pyi, di,
                                  rfc5903_384_bit_Random_ECP_Group_definition)

        assert Rxi == rfc5903_384_bit_Random_ECP_Group_test_vectors['gixi']
        assert Ryi == rfc5903_384_bit_Random_ECP_Group_test_vectors['giyi']


    def test_public_key(self):
        Rxi, Ryi = public_key(rfc5903_384_bit_Random_ECP_Group_test_vectors['ii'],
                              rfc5903_384_bit_Random_ECP_Group_definition,
                              rfc5903_384_bit_Random_ECP_Group_binary_powers_xi,
                              rfc5903_384_bit_Random_ECP_Group_binary_powers_yi)

        assert Rxi == rfc5903_384_bit_Random_ECP_Group_test_vectors['gixi']
        assert Ryi == rfc5903_384_bit_Random_ECP_Group_test_vectors['giyi']

        d = bytes.fromhex("0af857beff08046f23b03c4299eda86490393bde88e4f74348886b200555276b93b37d4f6fdec17c0ea581a30c59c727")
        di = int.from_bytes(d, byteorder='big', signed=False)
        Qx = bytes.fromhex("00ea9d109dbaa3900461a9236453952b1f1c2a5aa12f6d500ac774acdff84ab7cb71a0f91bcd55aaa57cb8b4fbb3087d")
        Qxi = int.from_bytes(Qx, byteorder='big', signed=False)
        Qy = bytes.fromhex("0fc0e3116c9e94be583b02b21b1eb168d8facf3955279360cbcd86e04ee50751054cfaebcf542538ac113d56ccc38b3e")
        Qyi = int.from_bytes(Qy, byteorder='big', signed=False)

        Rxi, Ryi = public_key(di,
                              rfc5903_384_bit_Random_ECP_Group_definition,
                              rfc5903_384_bit_Random_ECP_Group_binary_powers_xi,
                              rfc5903_384_bit_Random_ECP_Group_binary_powers_yi)

        assert Rxi == Qxi
        assert Ryi == Qyi