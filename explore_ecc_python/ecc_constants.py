"""
 ▄▄▄▄▄▄▄▄  ▄      ▄  ▄▄▄▄▄▄▄▄            ▄▄▄▄▄▄▄▄  ▄      ▄
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌▌   ▐░▌▐░█▀▀▀▀▀▀            ▀▀▀██▀▀▀ ▐░▌▌   ▐░▌
▐░▌       ▐░▌▐   ▐░▌▐░▌                     ▐▌    ▐░▌▐   ▐░▌
▐░█▄▄▄▄▄▄ ▐░▌ ▌  ▐░▌▐░▌ ▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄   ▐▌    ▐░▌ ▌  ▐░▌
▐░░░░░░░░▌▐░▌ ██ ▐░▌▐░▌▐░░░░░▌▐░░░░░░░░░░▌  ▐▌    ▐░▌ ██ ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌  ▐ ▐░▌▐░▌ ▀▀▀█░▌ ▀▀▀▀▀▀▀▀▀▀   ▐▌    ▐░▌  ▐ ▐░▌
▐░▌       ▐░▌   ▌▐░▌▐░▌    ▐░▌              ▐▌    ▐░▌   ▌▐░▌
▐░█▄▄▄▄▄▄ ▐░▌   ▐▐░▌▐░█▄▄▄▄█░▌           ▄▄▄██▄▄▄ ▐░▌   ▐▐░▌
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
 ▀▀▀▀▀▀▀▀  ▀      ▀  ▀▀▀▀▀▀▀▀            ▀▀▀▀▀▀▀▀  ▀      ▀
     45       6E        67        2D        49        6E
 01000101  01101110  01100111  00101101  01001001  01101110
           ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄      ▄
          ▐░░░░░░░░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
          ▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░▌▌  ▐▐░▌
          ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌▐░░▌ ░▌
          ▐░█▄▄▄▄█░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░░░░░░░░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░█▀█░█▀▀ ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌  ▐░▌  ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌   ▐░▌ ▐░█▄▄▄▄█░▌▐░█▄▄▄▄█░▌▐░▌    ▐░▌
          ▐░▌    ▐░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
           ▀      ▀  ▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀  ▀      ▀
              52        6F        6F        6D
           01010010  01101111  01101111  01101101

Logo edited from awesomeness:
    https://patorjk.com/software/taag/#p=display&f=Electronic&t=EngIn%20Room

(C) Geoff Krechting 2021
"""

from explore_ecc_python.ecc_functions import (
    point_double,
)


rfc5903_384_bit_Random_ECP_Group_definition = {
    "p": bytes.fromhex("FFFFFFFF FFFFFFFF FFFFFFFF FFFFFFFF FFFFFFFF FFFFFFFF FFFFFFFF FFFFFFFE FFFFFFFF 00000000 00000000 FFFFFFFF"),
    "ai": -3,
    "b": bytes.fromhex("B3312FA7 E23EE7E4 988E056B E3F82D19 181D9C6E FE814112 0314088F 5013875A C656398D 8A2ED19D 2A85C8ED D3EC2AEF"),
    "c": bytes.fromhex("FFFFFFFF FFFFFFFF FFFFFFFF FFFFFFFF FFFFFFFF FFFFFFFF C7634D81 F4372DDF 581A0DB2 48B0A77A ECEC196A CCC52973"),
    "seed": bytes.fromhex("A335926A A319A27A 1D00896A 6773A482 7ACDAC73"),
    "gx": bytes.fromhex("AA87CA22 BE8B0537 8EB1C71E F320AD74 6E1D3B62 8BA79B98 59F741E0 82542A38 5502F25D BF55296C 3A545E38 72760AB7"),
    "gy": bytes.fromhex("3617DE4A 96262C6F 5D9E98BF 9292DC29 F8F41DBD 289A147C E9DA3113 B5F0B8C0 0A60B1CE 1D7E819D 7A431D7C 90EA0E5F"),
}
rfc5903_384_bit_Random_ECP_Group_definition["pi"] = int.from_bytes(rfc5903_384_bit_Random_ECP_Group_definition['p'],
                                                                   byteorder='big', signed=False)
rfc5903_384_bit_Random_ECP_Group_definition["bi"] = int.from_bytes(rfc5903_384_bit_Random_ECP_Group_definition['b'],
                                                                   byteorder='big', signed=False)
rfc5903_384_bit_Random_ECP_Group_definition["ci"] = int.from_bytes(rfc5903_384_bit_Random_ECP_Group_definition['c'],
                                                                   byteorder='big', signed=False)
rfc5903_384_bit_Random_ECP_Group_definition["gxi"] = int.from_bytes(rfc5903_384_bit_Random_ECP_Group_definition['gx'],
                                                                    byteorder='big', signed=False)
rfc5903_384_bit_Random_ECP_Group_definition["gyi"] = int.from_bytes(rfc5903_384_bit_Random_ECP_Group_definition['gy'],
                                                                    byteorder='big', signed=False)
p384_curve_definition = rfc5903_384_bit_Random_ECP_Group_definition

rfc5903_384_bit_Random_ECP_Group_binary_powers_xi = [rfc5903_384_bit_Random_ECP_Group_definition['gxi']]
rfc5903_384_bit_Random_ECP_Group_binary_powers_yi = [rfc5903_384_bit_Random_ECP_Group_definition['gyi']]

for i in range(1, 384):
    Rxi, Ryi = point_double(rfc5903_384_bit_Random_ECP_Group_binary_powers_xi[i - 1],
                            rfc5903_384_bit_Random_ECP_Group_binary_powers_yi[i - 1],
                            rfc5903_384_bit_Random_ECP_Group_definition)
    rfc5903_384_bit_Random_ECP_Group_binary_powers_xi.append(Rxi)
    rfc5903_384_bit_Random_ECP_Group_binary_powers_yi.append(Ryi)

