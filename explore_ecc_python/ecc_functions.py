"""
 ▄▄▄▄▄▄▄▄  ▄      ▄  ▄▄▄▄▄▄▄▄            ▄▄▄▄▄▄▄▄  ▄      ▄
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌▌   ▐░▌▐░█▀▀▀▀▀▀            ▀▀▀██▀▀▀ ▐░▌▌   ▐░▌
▐░▌       ▐░▌▐   ▐░▌▐░▌                     ▐▌    ▐░▌▐   ▐░▌
▐░█▄▄▄▄▄▄ ▐░▌ ▌  ▐░▌▐░▌ ▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄   ▐▌    ▐░▌ ▌  ▐░▌
▐░░░░░░░░▌▐░▌ ██ ▐░▌▐░▌▐░░░░░▌▐░░░░░░░░░░▌  ▐▌    ▐░▌ ██ ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌  ▐ ▐░▌▐░▌ ▀▀▀█░▌ ▀▀▀▀▀▀▀▀▀▀   ▐▌    ▐░▌  ▐ ▐░▌
▐░▌       ▐░▌   ▌▐░▌▐░▌    ▐░▌              ▐▌    ▐░▌   ▌▐░▌
▐░█▄▄▄▄▄▄ ▐░▌   ▐▐░▌▐░█▄▄▄▄█░▌           ▄▄▄██▄▄▄ ▐░▌   ▐▐░▌
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
 ▀▀▀▀▀▀▀▀  ▀      ▀  ▀▀▀▀▀▀▀▀            ▀▀▀▀▀▀▀▀  ▀      ▀
     45       6E        67        2D        49        6E
 01000101  01101110  01100111  00101101  01001001  01101110
           ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄      ▄
          ▐░░░░░░░░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
          ▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░▌▌  ▐▐░▌
          ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌▐░░▌ ░▌
          ▐░█▄▄▄▄█░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░░░░░░░░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░█▀█░█▀▀ ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌  ▐░▌  ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌   ▐░▌ ▐░█▄▄▄▄█░▌▐░█▄▄▄▄█░▌▐░▌    ▐░▌
          ▐░▌    ▐░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
           ▀      ▀  ▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀  ▀      ▀
              52        6F        6F        6D
           01010010  01101111  01101111  01101101

Logo edited from awesomeness:
    https://patorjk.com/software/taag/#p=display&f=Electronic&t=EngIn%20Room

(C) Geoff Krechting 2021
"""


def euclidean_algorithm(a: int, b: int):
    if 0 > b:
        raise ValueError("Euclidean Algorithm input integers must be 0 < b < a")
    if b > a:
        raise ValueError("Euclidean Algorithm input integers must be 0 < b < a")
    j = 0
    q = [-1]
    r = [b]
    while r[j] != 0:
        q.append(a // b)
        r.append(a % b)
        a = b
        b = r[j+1]
        j += 1

    return (a, q, r, j)


def greatest_common_divisor(a: int, b: int):
    return euclidean_algorithm(a, b)


def gcd(a: int, b: int):
    return euclidean_algorithm(a, b)


def modular_multiplicative_inverse(a: int, m: int):
    if a == 1:
        return 1
    # A linear congruence ax = b (mod m) implies that a = b/x (mod m)
    # If d is the greatest common divisor of a and m, then ax = b (mod m) has exactly d solutions if and only if d divide b.
    (d, q, r, j) = euclidean_algorithm(m, a % m)
    if d != 1:
        return False
    # In the case that b = 1, ax = 1 (mod m), x = 1/a (mod m), and 1 = ax (mod m)
    # Therefore x is the modular multiplicative inverse of a.
    # Therefore x is the sole modular multiplicative inverse of a, as b = 1, and d must therefore also be 1.
    # Back substituting into the Euclidean Algorithm division algorithm rounds re-arranged to solve for the remainders:
    x = 1
    y = -q[j - 1]
    for i in range(j - 2, 0, -1):
        _x = y
        y = x - q[i] * y
        x = _x
    return y % m


def mmi(a: int, m: int):
    return modular_multiplicative_inverse(a, m)


def point_is_on_curve(Pxi: int, Pyi: int,
                      curve_definition: dict):
    pi = curve_definition['pi']
    lhs = (Pyi ** 2) % pi
    rhs = ((Pxi ** 3) + (Pxi * curve_definition['ai']) + curve_definition['bi']) % pi
    return lhs == rhs


def point_double(Pxi: int, Pyi: int,
                 curve_definition: dict):
    pi = curve_definition['pi']
    s1 = (3 * (Pxi ** 2) + curve_definition['ai']) % pi
    #s2 = pow(2 * Pyi, -1, pi)
    s2 = mmi(2 * Pyi, pi)
    s = (s1 * s2) % pi
    Rxi = ((s ** 2) - (2 * Pxi)) % pi
    Ryi = (s * (Pxi - Rxi) - Pyi) % pi

    return Rxi, Ryi


def point_add(Pxi: int, Pyi: int,
              Qxi: int, Qyi: int,
              curve_definition: dict):
    if (Pxi == Qxi) and (Pyi == Qyi):
        Rxi, Ryi = point_double(Pxi, Pyi, curve_definition)
    else:
        pi = curve_definition['pi']
        s1 = (Qyi - Pyi) % pi
        #s2 = pow(Qxi - Pxi, -1, pi)
        s2 = mmi(Qxi - Pxi, pi)
        s = (s1 * s2) % pi
        Rxi = ((s ** 2) - Pxi - Qxi) % pi
        Ryi = (s * (Pxi - Rxi) - Pyi) % pi

    return Rxi, Ryi


def point_multiply(Pxi: int, Pyi: int, di: int,
                   curve_definition: dict):
    pi = curve_definition['pi']

    Pxi_p_2_i = [Pxi]
    Pyi_p_2_i = [Pyi]
    for i in range(1, 384):
        Rxi, Ryi = point_double(Pxi_p_2_i[i - 1], Pyi_p_2_i[i - 1],
                                curve_definition)
        Pxi_p_2_i.append(Rxi)
        Pyi_p_2_i.append(Ryi)

    dbits = []
    for j in range(0, 384):
        if (0x01 << j) & di != 0:
            dbits.append(j)

    Txi = Pxi_p_2_i[dbits[0]]
    Tyi = Pyi_p_2_i[dbits[0]]
    for dbit in range(1, len(dbits)):
        Rxi, Ryi = point_add(Txi, Tyi,
                             Pxi_p_2_i[dbits[dbit]], Pyi_p_2_i[dbits[dbit]],
                             curve_definition)
        Txi = Rxi
        Tyi = Ryi

    return Rxi, Ryi


def public_key(di: int,
               curve_definition: dict,
               curve_powers_xi: list,
               curve_powers_yi: list):
    dbits = []
    for j in range(0, 384):
        if (0x01 << j) & di != 0:
            dbits.append(j)

    Txi = curve_powers_xi[dbits[0]]
    Tyi = curve_powers_yi[dbits[0]]
    for dbit in range(1, len(dbits)):
        Rxi, Ryi = point_add(Txi, Tyi,
                             curve_powers_xi[dbits[dbit]], curve_powers_yi[dbits[dbit]],
                             curve_definition)
        Txi = Rxi
        Tyi = Ryi

    return Rxi, Ryi
